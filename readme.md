<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

# Teste SmartRights

Aplicação de teste feita com as seguintes tecnologias:
  - Laravel Framework 5.8.3
  - Apache/2.4.34
  - PHP 7.3.3
  - MongoDB v4.0.3
  - jenssegers/laravel-mongodb

Funções disponíveis:
  - Listar usuários
  - Buscar pelo nome ou sobrenome
  - Criar usuários
  - Atualizar um usuário específico
  - Remover um usuário

# Documentação

### Endpoints

O serviço está hospedado no Google Cloud.

| FUNÇAO | HTTP METHOD | ENDPOINT |
| ------ | ------ | ------ |
| Listar | GET | http://35.198.48.53/api/contacts |
| Criar | POST | http://35.198.48.53/api/contacts |
| Atualizar | PUT | http://35.198.48.53/api/contacts/<id_parameter> |
| Remover | DELETE | http://35.198.48.53/api/contacts/<id_parameter> |
| Buscar | POST | http://35.198.48.53/api/contacts/search |

### Listar usuários
Para listar os usuários, basta fazer uma chamada GET para o endereço correspondente.
Response de exemplo:

```json
{
    "data": [
        {
            "id": "5c8531d9df2d4a055d78f2c2",
            "name": "Jorge",
            "surname": "Santana",
            "phones": [
                {
                    "name": "Celular",
                    "number": "(21)999999999"
                }
            ]
        }
    ]
}
```
### Criar usuário
Para criar um usuário, basta fazer uma chamada POST com os seguintes parametros:

#### Headers:
    Content-Type: application/json

#### Body
Todos os parametros são obrigatórios
```json
{
	"name": "Jorge",
	"surname": "Santana",
	"birthday": "30/06/1991",
	"phones": [
        {
            "name": "Celular",
            "number": "(21)999999999"
        }
    ]
}
```

Response de exemplo:

```json
{
    "data": {
        "id": "5c8574ebfdf6d3b2c65e78d5",
        "name": "Jorge",
        "surname": "Santana",
        "phones": [
            {
                "name": "Celular",
                "number": "(21)99999999"
            }
        ]
    },
    "success": true
}
```

### Atualizar usuário
Para atualizar um usuário, basta fazer uma chamada PUT passando o ID do usuário como parâmetro na URL e o body com os dados:

#### Headers:
    Content-Type: application/json

#### Body
Todos os parametros são obrigatórios. Se quiser remover um numero de telefone basta não passar esse número no JSON, mas é obrigado a ter pelo menos um.
```json
{
	"name": "Jorge",
	"surname": "Santana",
	"birthday": "30/06/1991",
	"phones": [
        {
            "name": "Celular",
            "number": "(21)999999999"
        },
        {
            "name": "Casa",
            "number": "(21)23944827"
        }
    ]
}
```

Response de exemplo:

```json
{
    "data": {
        "id": "5c8574ebfdf6d3b2c65e78d5",
        "name": "Jorge",
        "surname": "Santana",
        "phones": [
            {
                "name": "Celular",
                "number": "(21)99999999"
            },
            {
                "name": "Casa",
                "number": "(21)23944827"
            }
        ]
    },
    "success": true
}
```

### Remover um usuário
Para remover um usuário, basta fazer uma chamada DELETE passando o ID do usuário como parâmetro na URL. O retorno da chamada é um HTTP status 204 - no content.

#### Body
Não há


### Buscar usuário
Para fazer uma busca de um usuário, basta fazer uma chamada POST. 
A busca é feita pelo nome ou sobrenome do usuário. Lembrando que é case sensitive.

#### Headers:
    Content-Type: application/json

#### Body
Todos os parametros são obrigatórios
```json
{
	"name": "Jorge"
}
```

Response de exemplo:

```json
{
    "data": {
        "id": "5c8574ebfdf6d3b2c65e78d5",
        "name": "Jorge",
        "surname": "Santana",
        "phones": [
            {
                "name": "Celular",
                "number": "(21)99999999"
            },
            {
                "name": "Casa",
                "number": "(21)23944827"
            }
        ]
    },
    "success": true
}
```
