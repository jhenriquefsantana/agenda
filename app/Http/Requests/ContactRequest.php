<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequest as CustomFormRequest;
use Illuminate\Http\JsonResponse;
use App\Contact;

class ContactRequest extends CustomFormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST':
                return [
                    'name'          => 'required',
                    'surname'       => 'required',
                    'birthday'      => 'required|date_format:d/m/Y',
                    'phones'         => 'required',
                    'phones.*.name'    => 'required',
                    'phones.*.number'  => 'required',
                ];
                break;            
            default:
                return [];
                break;
        }
    }
}
