<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Http\Resources\ContactResource;
use App\Http\Requests\ContactRequest;

class ContactController extends BaseController
{
    public function index($id = null) {

    	if($id == null) {
    		return ContactResource::collection(Contact::all());
    	} else {
    		$contact = $this->show($id);

    		if(!$contact->resource) {
				return $this->responseError("not found", "404");
			}

			return $contact;
    	}
    }

    public function search(Request $request) {
    	
    	if(!$request->name) {
    		return response(['error' => 'name is required for this method']);
    	}
    	
    	// Criando array com nome e sobrenome
    	$names = explode(" ", $request->name);
		
		$contacts = Contact::whereIn('name', $names)->orWhereIn('surname', $names)->get();

		return ContactResource::collection($contacts);
	}

	public function show($id) {
		return new ContactResource(Contact::find($id));
	}

	public function create(ContactRequest $request) {

		$data = $this->readJsonFromRequest();

		if($data) {
			
			return (new ContactResource(Contact::create($request->all())))->response()->setStatusCode(200);
		}
	}

	public function update(ContactRequest $request, $id) {
		
		$data = $this->readJsonFromRequest();

		if($data) {
			$contact = $this->show($id);
			if(!$contact->resource) {
				return $this->responseError("not found", "404");
			}
			$contact->update($request->all());

			return new ContactResource($contact);
		}
	}

	public function destroy($id) {
		$contact = $this->show($id);

		if(!$contact->resource) {
			return $this->responseError("not found", "404");
		}

		$contact->delete();

		return response("", 204);
	}
}
