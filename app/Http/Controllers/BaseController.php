<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
	/**
	 * lê a partir do body recebido no request, um bloco json.
	 * 
	 * @return null|array  - nulo caso não consiga converter o json, array associativo com dados do json em caso de sucesso
	 */
	protected function readJsonFromRequest()
	{
		$json = file_get_contents('php://input');	
		$data = json_decode($json, true);
				
		if($data === null && json_last_error() !== JSON_ERROR_NONE)
		{
			return null;
		}
		else
		{
			return $data;
		}
	}

	protected function responseError($message, $code) {
		$json = [
                        'success' => false,
                        'error' => [
                            'code' => $code,
                            'message' => $message,
                        ],
                    ];
		return response()->json($json, $code);
	}
}
