<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Contact extends Eloquent
{
    protected $fillable = ['name', 'surname', 'birthday', 'phones'];

	public function getFullnameAttribute()
	{
		return "{$this->name} {$this->surname}";
	}
}
