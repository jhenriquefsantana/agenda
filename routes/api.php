<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('contacts/{id?}', 'ContactController@index')->name('contact.index');
Route::post('contacts', 'ContactController@create')->name('contact.create');
Route::post('contacts/search', 'ContactController@search')->name('contact.search');
Route::put('contacts/{id}', 'ContactController@update')->name('contact.update');
Route::delete('contacts/{id}', 'ContactController@destroy')->name('contact.delete');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
